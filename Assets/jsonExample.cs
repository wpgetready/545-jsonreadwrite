﻿using UnityEngine;
using UnityEngine.UI; //Usado para referencia el boton
using System.IO; //Usado para acceder a los archivos

public class jsonExample : MonoBehaviour {

    //Explico la técnica en varios pasos para que se entienda. LEERLO DETENIDAMENTE, es la clave de todo!
    //1-Cada dispositivo (Android / IOS o Windows) tiene un directorio donde se puede leer y grabar datos. Unity se refiere a este directorio como Application.persistentDataPath
    //Referencia: https://docs.unity3d.com/ScriptReference/Application-persistentDataPath.html

    //2- Esta carpeta se usa para almacenar datos que se persisten a lo largo de la aplicación. 

    //3- Cuando una aplicación se instala por primera vez, en esta carpeta no hay NADA de esta aplicación, ni se instala NADA.
    //NOTAR QUE DIGO 'INSTALAR': instalar quiere decir que el programa se instaló pero NUNCA CORRIO SIQUIERA UNA VEZ. Por lo tanto, al instalarse, no se puede poner nada en persistentpath, 
    //esto se hace cuando al menos se corre una vez la aplicacion.

    //4- Lo que hace una aplicación para persistir sus datos es:
    //          -Leer las configuraciones que desea persistir (por ejemplo, accediendo a Resources)
    //          -Grabando los datos que desea persistir en Application.PersistentDataPath
    //          -Luego, lo que hace es VERIFICAR si tiene datos en PersistentDataPath. Si NO tiene, los lee de Resources, en caso contrario se lee de Application.persistenDataPath.
    

    //5-La carpeta Application.PersistentDataPath cambia por dispositivo, ya sea Windows, Mac, Android , Linux etc.
    //Asi que para resetear los datos manualmente hay que saber donde ir. Esta es una carpeta que se puede borrar manualmente
    //Referencia: https://answers.unity.com/questions/1144351/cant-find-applicationpersistentdatapath-location.html


    public Text txt; //Este objeto lo usamos para mostrar que la PRIMERA vez se leen los datos de Resources. Si se corre la app por SEGUNDA vez, lee los datos de PersistentDataPath
    //Para resetear, es preciso ir a la carpeta correspondiente y borrar los archivos. En Android al desinstalar o borrar los archivos (está dicha opcion en la ventana de desinstalación) tambien se puede hacer.

    void Start () {

        string jsonData;
        data myData;

        if (File.Exists(Application.persistentDataPath + "/myJsonFile.json"))
        {
            //Paso 0: PRIMERO verifico si la aplicacion alguna vez grabo algo en el persistentDataPath
            //Si asi lo hizo, leo los datos ahi, e IGNORO los archivos en Resource File

            jsonData = File.ReadAllText(Application.persistentDataPath + "/myJsonFile.json");
            //Se acabó. Ya tengo la configuración con los cambios.
            myData = JsonUtility.FromJson<data>(jsonData);

            txt.text = myData.playerName + " leido desde persistent DataPath!" ; //muestro los datos en el boton, el BOTON NO HACE NADA LO PUSE DE EJEMPLO!
        } else
        {
            //POR ACA LLEGO SI ES LA PRIMERA VEZ QUE CORRO LA APP O BORRO LOS DATOS DE LA CARPETA PERSISTENTDATAPATH

            //Paso 1: Obtener el archivo en formato Json, como un string, observar que la extension .json SE ASUME y NO debe agregarse.
            //SE ASUME tambien que existe una carpeta Resources/
            //Por lo tanto el archivo se encontrará en Resources/SetupData/myJsonFile.json
            // Referencia: https://answers.unity.com/questions/1092940/loading-json-as-a-text-asset-using-resourcesload.html

            jsonData = Resources.Load<TextAsset>("SetupData/myJsonFile").text;

            //2-Siguiente paso: convertir texto a un objeto JSON
            //Para ello, se necesita una clase que permita tomar este string y convertir en un objeto. Para eso esta la clase data, que tenemos que armar nosotros, ver data.cs
            //Si queremos convertir de un string JSON a clases leer: https://stackoverflow.com/questions/48609991/unity-c-parse-json-file-into-data-structure

            myData = JsonUtility.FromJson<data>(jsonData);

            txt.text = myData.playerName + " leido desde los Resources!";

            //3-Tenemos la clase levantada, podemos realizar modificaciones
            myData.level = 999;
            myData.playerName = "Apocalipsis";
            myData.timeElapsed = 50.2f; //f indica que es un float.

            //4-Ahora reconvertirmos a json string

            string newJson = JsonUtility.ToJson(myData);

            //5- Salvar resultado.
            File.WriteAllText(Application.persistentDataPath + "/myJsonFile.json", newJson.ToString());
        }
    }
}
