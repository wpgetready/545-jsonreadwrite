﻿using System;
//Esta es la clase de ejemplo
// Observar que los campos coinciden con el Json
// {"level":1,"timeElapsed":47.5,"playerName":"Dr Charles Francis"}
//Otros detalles:
// Esta clase SOLO necesita usar un using hacia System porque usa [Seriablizable]. Despues NADA MAS, es una clase que contiene una estructura de datos.

[Serializable]
public class data
{
    public int level;
    public float timeElapsed;
    public string playerName;
}
